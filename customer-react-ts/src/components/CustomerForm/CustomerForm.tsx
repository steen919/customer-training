import { ReactElement } from "react";
import { Field, Form } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { PostCustomer } from "../../actions/CustomerActions";
import { FormValues } from "../../actions/CustomerActionTypes";
import { RootStore } from "../../Store";
import "./CustomerForm.css";

const CustomerForm: React.FC = (): ReactElement => {
  const dispatch = useDispatch();
  const customerState = useSelector((state: RootStore) => state.customerState);

  const onSubmit = async (values: FormValues) => {
    dispatch(PostCustomer(values));
    window.location.href = "/#/list";
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={(values) => {
        const errors: FormValues = {};
        if (!values.firstName) {
          errors.firstName = "Förnamn är obligatoriskt";
        }
        if (!values.lastName) {
          errors.lastName = "Efternamn är obligatoriskt";
        }
        if (!values.age) {
          errors.age = "Ålder är obligatoriskt ";
        }
        if (values.lastName?.includes("fis")) {
          errors.lastName = "Efternamn får inte innehålla 'fis'";
        }
        if (
          !(typeof values.age === "string" && Number.parseInt(values.age) > 0)
        ) {
          errors.age = "Ålder måste vara över 0";
        }
        return errors;
      }}
    >
      {({ handleSubmit, form, submitting, pristine, invalid }) => (
        <form onSubmit={handleSubmit} className="customerForm">
          <Field name="firstName">
            {({ input, meta }) => (
              <div>
                <label>Förnamn</label>
                <input {...input} type="text" />
                {meta.error && meta.touched && (
                  <div className="error">{meta.error}</div>
                )}
              </div>
            )}
          </Field>
          <Field name="lastName">
            {({ input, meta }) => (
              <div>
                <label>Efternamn</label>
                <input {...input} type="text" />
                {meta.error && meta.touched && (
                  <div className="error">{meta.error}</div>
                )}
              </div>
            )}
          </Field>
          <Field name="age">
            {({ input, meta }) => (
              <div>
                <label>Ålder</label>
                <input {...input} type="number" />
                {meta.error && meta.touched && (
                  <div className="error">{meta.error}</div>
                )}
              </div>
            )}
          </Field>
          <button type="submit" disabled={submitting || pristine || invalid}>
            Skicka
          </button>
          {customerState.posted && <div>Sparad!</div>}
        </form>
      )}
    </Form>
  );
};

export default CustomerForm;
