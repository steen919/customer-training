import { ReactElement, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetCustomer } from "../../actions/CustomerActions";
import {
  Customer,
  instanceOfCustomers,
} from "../../actions/CustomerActionTypes";
import "./CustomerList.css";
import { RootStore } from "../../Store";

const CustomerList: React.FC = (): ReactElement => {
  const dispatch = useDispatch();
  const customerState = useSelector((state: RootStore) => state.customerState);

  useEffect(() => {
    dispatch(GetCustomer());
  }, []);

  return (
    <div className="CustomerList">
      {customerState.loading && <div>Laddar...</div>}
      {customerState.customers && instanceOfCustomers(customerState.customers) && (
        <div>
          <table>
            <tbody>
              {customerState.customers.map((customer: Customer) => (
                <tr key={customer.id}>
                  <td>{customer.firstName}</td>
                  <td>{customer.lastName}</td>
                  <td>{customer.age}</td>
                </tr>
              ))}
              <tr className="totals-row">
                <td colSpan={2}>Total ålder</td>
                <td>{customerState.totals}</td>
              </tr>
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default CustomerList;
