import { Dispatch } from "redux";
import store from "../Store";
import axios from "axios";
import {
  CustomerDispatchTypes,
  CUSTOMER_GET_LOADING,
  CUSTOMER_GET_FAIL,
  CUSTOMER_GET_SUCCESS,
  CustomerData,
  CUSTOMER_POST_LOADING,
  CUSTOMER_POST_SUCCESS,
  CUSTOMER_POST_FAIL,
  FormValues,
} from "./CustomerActionTypes";

const apiUrl = store.getState().configState.apiUrl;

export const GetCustomer = () => async (
  dispatch: Dispatch<CustomerDispatchTypes>
): Promise<void> => {
  try {
    dispatch({
      type: CUSTOMER_GET_LOADING,
    });

    const res = await axios.get<CustomerData>(apiUrl);

    dispatch({
      type: CUSTOMER_GET_SUCCESS,
      payload: res.data,
    });
  } catch (e) {
    dispatch({
      type: CUSTOMER_GET_FAIL,
    });
  }
};

export const PostCustomer = (values: FormValues) => async (
  dispatch: Dispatch<CustomerDispatchTypes>
): Promise<void> => {
  try {
    dispatch({
      type: CUSTOMER_POST_LOADING,
    });
    const res = await axios.post(apiUrl, values);
    dispatch({
      type: CUSTOMER_POST_SUCCESS,
      payload: res.data,
    });
  } catch (e) {
    dispatch({
      type: CUSTOMER_POST_FAIL,
    });
  }
};
