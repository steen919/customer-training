export const CUSTOMER_GET_LOADING = "CUSTOMER_GET_LOADING";
export const CUSTOMER_GET_FAIL = "CUSTOMER_GET_FAIL";
export const CUSTOMER_GET_SUCCESS = "CUSTOMER_GET_SUCCESS";

export const CUSTOMER_POST_LOADING = "CUSTOMER_POST_LOADING";
export const CUSTOMER_POST_FAIL = "CUSTOMER_POST_FAIL";
export const CUSTOMER_POST_SUCCESS = "CUSTOMER_POST_SUCCESS";

export type CustomerData = {
  data: Customer[] | Customer;
  status: string;
  message?: string;
};

export type Customer = {
  firstName: string;
  lastName: string;
  age: number;
  id?: number;
};

export type FormValues = {
  firstName?: string;
  lastName?: string;
  age?: string;
};

export function instanceOfCustomer(
  object: Customer | Customer[]
): object is Customer {
  return !Array.isArray(object);
}

export function instanceOfCustomers(
  object: Customer | Customer[]
): object is Customer[] {
  return Array.isArray(object);
}

export interface CustomerGetLoading {
  type: typeof CUSTOMER_GET_LOADING;
}

export interface CustomerGetFail {
  type: typeof CUSTOMER_GET_FAIL;
}

export interface CustomerGetSuccess {
  type: typeof CUSTOMER_GET_SUCCESS;
  payload: CustomerData;
}

export interface CustomerPostLoading {
  type: typeof CUSTOMER_POST_LOADING;
}

export interface CustomerPostFail {
  type: typeof CUSTOMER_POST_FAIL;
}

export interface CustomerPostSuccess {
  type: typeof CUSTOMER_POST_SUCCESS;
  payload: CustomerData;
}

export type CustomerDispatchTypes =
  | CustomerGetLoading
  | CustomerGetFail
  | CustomerGetSuccess
  | CustomerPostLoading
  | CustomerPostFail
  | CustomerPostSuccess;
