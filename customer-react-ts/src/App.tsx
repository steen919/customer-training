import React, { ReactElement } from "react";
import "./App.css";
import { Route, NavLink, HashRouter } from "react-router-dom";
import CustomerForm from "./components/CustomerForm/CustomerForm";
import CustomerList from "./components/CustomerList/CustomerList";

const App: React.FC = (): ReactElement => {
  return (
    <HashRouter>
      <div className="app">
        <h1>Kundhanteraren</h1>
        <ul className="header">
          <li>
            <NavLink exact to="/">
              Formulär
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/list">
              Lista
            </NavLink>
          </li>
        </ul>
        <Route exact path="/" component={CustomerForm} />
        <Route path="/list" component={CustomerList} />
      </div>
    </HashRouter>
  );
};

export default App;
