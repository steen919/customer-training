import {
  Customer,
  CustomerData,
  CustomerDispatchTypes,
  CUSTOMER_GET_FAIL,
  CUSTOMER_GET_LOADING,
  CUSTOMER_GET_SUCCESS,
  CUSTOMER_POST_FAIL,
  CUSTOMER_POST_LOADING,
  CUSTOMER_POST_SUCCESS,
  instanceOfCustomer,
} from "../actions/CustomerActionTypes";

interface CustomerState {
  loading: boolean;
  customers?: CustomerData["data"];
  totals: number;
  posted?: boolean;
  lastPostedCustomer?: CustomerData["data"];
}

const defaultState: CustomerState = {
  loading: false,
  totals: 0,
};

const customerReducer = (
  state: CustomerState = defaultState,
  action: CustomerDispatchTypes
): CustomerState => {
  switch (action.type) {
    case CUSTOMER_GET_FAIL:
      return {
        loading: false,
        totals: state.totals,
      };
    case CUSTOMER_GET_LOADING:
      return {
        loading: true,
        totals: state.totals,
      };
    case CUSTOMER_GET_SUCCESS:
      return {
        loading: false,
        customers: action.payload["data"],
        totals: calcTotals(action.payload["data"]),
      };
    case CUSTOMER_POST_FAIL:
      return {
        loading: false,
        totals: state.totals,
      };
    case CUSTOMER_POST_LOADING:
      return {
        loading: true,
        totals: state.totals,
      };
    case CUSTOMER_POST_SUCCESS:
      return {
        loading: false,
        customers: state.customers,
        totals: state.totals,
        posted: true,
        lastPostedCustomer: action.payload["data"],
      };
    default:
      return state;
  }
};

const calcTotals = (customers: Customer[] | Customer) => {
  let sum = 0;
  if (instanceOfCustomer(customers)) {
    if (customers.age != null) {
      sum += customers.age;
    }
  } else {
    customers.forEach((a: Customer) => {
      sum += a.age;
    });
  }
  return sum;
};

export default customerReducer;
