import { combineReducers } from "redux";
import configReducer from "./ConfigReducer";
import customerReducer from "./CustomerReducer";

const RootReducers = combineReducers({
  customerState: customerReducer,
  configState: configReducer,
});

export default RootReducers;
