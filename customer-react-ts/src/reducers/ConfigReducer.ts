interface ConfigState {
  apiUrl: string;
}

const defaultState: ConfigState = {
  apiUrl: "http://localhost:8080/customers",
};

const configReducer = (): ConfigState => {
  return defaultState;
};

export default configReducer;
