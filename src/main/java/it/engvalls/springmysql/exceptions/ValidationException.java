package it.engvalls.springmysql.exceptions;

import lombok.Getter;
import lombok.NonNull;

import java.util.Map;

public class ValidationException extends RuntimeException {

    @Getter
    private final Map<String, String> errors;

    public ValidationException(@NonNull Map<String, String> errors) {
        super(errors.toString());
        this.errors = errors;
    }
}
