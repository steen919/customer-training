package it.engvalls.springmysql.services;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

@Service
public class JsonService {

    @NonNull
    public String toJson(@NonNull List<?> objects, @NonNull HttpStatus status, @Nullable Map<String, String> messages) {

        JSONObject json = new JSONObject();
        if (status.is2xxSuccessful()) {
            json.put("status", "success");
            if (status.equals(HttpStatus.CREATED)) {
                json.put("data", new JSONObject(objects.get(0)));
            } else {
                json.put("data", objects);
            }
        } else if (status.is4xxClientError()) {
            json.put("status", "fail");
            if (!ObjectUtils.isEmpty(messages)) {
                json.put("message", messages);
            } else {
                json.put("message", "Something went wrong");
            }
        } else {
            json.put("status", "error");
            json.put("message", "Something went wrong");
        }

        String response = json.toString();
        if (response == null) {
            return "{\"status\":\"error\",\"message\":\"Something went wrong\"}";
        }
        return response;
    }
}
