package it.engvalls.springmysql.services;

import it.engvalls.springmysql.exceptions.ValidationException;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Service
public class CustomerPostDataValidator {

    public void validate(@NonNull Map<String, String> data) throws ValidationException {

        Map<String, String> errors = new HashMap<>();
        List<String> requiredParameters = Arrays.asList(
                "firstName",
                "lastName",
                "age");

        if (!data.keySet().containsAll(requiredParameters)) {
            requiredParameters.forEach(requiredParameter -> {
                if (!data.containsKey(requiredParameter)) {
                    errors.put(requiredParameter, "Missing parameter");
                }
            });
        }

        requiredParameters.forEach(requiredParameter -> {
            String parameter = data.get(requiredParameter);
            if (ObjectUtils.isEmpty(parameter)) {
                errors.putIfAbsent(requiredParameter, "Missing value");
            }
            if (requiredParameter.equals("lastName") &&
                    !ObjectUtils.isEmpty(parameter) &&
                    parameter.toLowerCase(Locale.ROOT).contains("fis")
            ) {
                errors.putIfAbsent(requiredParameter, "Contains bad content (fis)");
            }
            if (requiredParameter.equals("age")) {
                try {
                    int age = Integer.parseInt(parameter.trim());
                    if (age < 0) {
                        throw new NumberFormatException();
                    }
                } catch (Exception e) {
                    errors.putIfAbsent(requiredParameter, "age (" + parameter + ") is not in correct format (should be positive integer)");
                }
            }
        });

        if (errors.size() > 0) {
            throw new ValidationException(errors);
        }
    }
}
