package it.engvalls.springmysql.models;

import lombok.Data;

@Data
public class Customer {

    private final long id;
    private final String firstName;
    private final String lastName;
    private final int age;
}
