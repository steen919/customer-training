package it.engvalls.springmysql.controllers;

import it.engvalls.springmysql.exceptions.ValidationException;
import it.engvalls.springmysql.models.Customer;
import it.engvalls.springmysql.repositories.CustomerRepository;
import it.engvalls.springmysql.services.CustomerPostDataValidator;
import it.engvalls.springmysql.services.JsonService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerRepository userRepo;
    private final CustomerPostDataValidator validator;
    private final JsonService jsonService;

    Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> retrieveAll() {
        try {
            List<Customer> customers = userRepo.retrieveAll();
            return new ResponseEntity<>(jsonService.toJson(customers, HttpStatus.OK, null), HttpStatus.OK);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    jsonService.toJson(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR, null),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Validated
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createPost(@RequestBody Map<String, String> customerData) {

        try {
            validator.validate(customerData);
        } catch (ValidationException exception) {
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    jsonService.toJson(Collections.emptyList(), HttpStatus.BAD_REQUEST, exception.getErrors()),
                    HttpStatus.BAD_REQUEST);
        }

        String firstName = customerData.get("firstName");
        String lastName = customerData.get("lastName");
        int age = Integer.parseInt(customerData.get("age"));

        Customer customer = userRepo.create(firstName, lastName, age);
        if (customer == null) {
            return new ResponseEntity<>(
                    jsonService.toJson(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR, null),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(jsonService.toJson(Collections.singletonList(customer), HttpStatus.CREATED, null),
                HttpStatus.CREATED);
    }
}
