package it.engvalls.springmysql.repositories;

import it.engvalls.springmysql.models.Customer;
import lombok.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;

public interface CustomerRepository {

    @NonNull
    List<Customer> retrieveAll();

    @Nullable
    Customer create(@NonNull String firstName, @NonNull String lastName, @NonNull int age);
}
