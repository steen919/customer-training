package it.engvalls.springmysql.repositories;

import it.engvalls.springmysql.models.Customer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class DatabaseCustomerRepository implements CustomerRepository {

    private final JdbcTemplate jdbc;

    @NonNull
    @Override
    public List<Customer> retrieveAll() {
        return jdbc.query(
                "SELECT id, first_name, last_name, age FROM customers",
                (rs, rowNum) -> new Customer(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("age")
                )
        );
    }

    @Nullable
    @Override
    public Customer create(@NonNull String firstName, @NonNull String lastName, int age) {

        final String INSERT_SQL = "INSERT INTO customers(first_name, last_name, age) VALUES (?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int rows = jdbc.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_SQL, new String[]{"id"});
                    ps.setString(1, firstName);
                    ps.setString(2, lastName);
                    ps.setInt(3, age);
                    return ps;
                },
                keyHolder);
        if (rows != 1) {
            return null;
        } else {
            return new Customer(Objects.requireNonNull(keyHolder.getKey()).longValue(), firstName, lastName, age);
        }
    }
}
