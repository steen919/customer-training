package it.engvalls.springmysql.services;

import it.engvalls.springmysql.models.Customer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonServiceTest {

    private final JsonService json = new JsonService();

    @Test
    void emptyListToJson() {
        String response = json.toJson(Collections.emptyList(), HttpStatus.OK, null);
        assertEquals("{\"data\":[],\"status\":\"success\"}", response);
    }

    @Test
    void oneCustomerInListToJson() {
        Customer customer = new Customer(1L, "Luke", "Skywalker", 31);
        String content = json.toJson(Collections.singletonList(customer), HttpStatus.OK, null);
        JSONObject json = new JSONObject(content);

        JSONArray data = json.getJSONArray("data");
        assertEquals(1, data.length());

        JSONObject customerData = data.getJSONObject(0);
        assertEquals(customer.getFirstName(), customerData.getString("firstName"));
        assertEquals(customer.getLastName(), customerData.getString("lastName"));
        assertEquals(customer.getAge(), customerData.getInt("age"));

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void twoCustomerInListToJson() {
        Customer customer = new Customer(1L, "Luke", "Skywalker", 31);
        String content = json.toJson(Arrays.asList(customer, customer), HttpStatus.OK, null);
        JSONObject json = new JSONObject(content);

        JSONArray data = json.getJSONArray("data");
        assertEquals(2, data.length());

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void oneCreatedCustomerInListToJson() {
        Customer customer = new Customer(1L, "Luke", "Skywalker", 31);
        String content = json.toJson(Collections.singletonList(customer), HttpStatus.CREATED, null);
        JSONObject json = new JSONObject(content);

        JSONObject data = json.getJSONObject("data");

        assertEquals(customer.getFirstName(), data.getString("firstName"));

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void badDataCustomerInListToJson() {
        Customer customer = new Customer(1L, "Luke", "Skyfiswalker", 31);
        Map<String, String> errors = new HashMap<>();
        errors.put("error", "Weird error");
        String content = json.toJson(Collections.singletonList(customer), HttpStatus.BAD_REQUEST, errors);
        JSONObject json = new JSONObject(content);

        JSONObject message = json.getJSONObject("message");
        assertEquals(errors.get("error"), message.getString("error"));

        String status = json.getString("status");
        assertEquals("fail", status);
    }

    @Test
    void serverErrorToJson() {
        String content = json.toJson(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR, null);
        JSONObject json = new JSONObject(content);

        assertEquals("Something went wrong", json.getString("message"));

        String status = json.getString("status");
        assertEquals("error", status);
    }
}