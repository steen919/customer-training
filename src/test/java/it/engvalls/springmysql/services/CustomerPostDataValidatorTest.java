package it.engvalls.springmysql.services;

import it.engvalls.springmysql.exceptions.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerPostDataValidatorTest {

    private final CustomerPostDataValidator validator = new CustomerPostDataValidator();

    @Test
    void validateCorrectCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{
                {"firstName", "Luke"},
                {"lastName", "Skywalker"},
                {"age", "31"},
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

        validator.validate(customer);
    }

    @Test
    void validateNullCustomer() {
        Assertions.assertThrows(NullPointerException.class, () -> validator.validate(null));
    }

    @Test
    void validateEmptyCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{})
                .collect(Collectors.toMap(data -> data[0], data -> data[1]));

        ValidationException exc = Assertions.assertThrows(ValidationException.class, () -> validator.validate(customer));

        assertEquals("Missing parameter", exc.getErrors().get("firstName"));
        assertEquals("Missing parameter", exc.getErrors().get("lastName"));
        assertEquals("Missing parameter", exc.getErrors().get("age"));
    }

    @Test
    void validateMissingLastNameCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{
                {"firstName", "Luke"},
                {"age", "31"},
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

        ValidationException exc = Assertions.assertThrows(ValidationException.class, () -> validator.validate(customer));

        assertEquals("Missing parameter", exc.getErrors().get("lastName"));
    }

    @Test
    void validateMissingValueForLastNameCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{
                {"firstName", "Luke"},
                {"lastName", ""},
                {"age", "31"},
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

        ValidationException exc = Assertions.assertThrows(ValidationException.class, () -> validator.validate(customer));

        assertEquals("Missing value", exc.getErrors().get("lastName"));
    }

    @Test
    void validateWrongAgeCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{
                {"firstName", "Luke"},
                {"lastName", "Skywalker"},
                {"age", "-1"},
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

        ValidationException exc = Assertions.assertThrows(ValidationException.class, () -> validator.validate(customer));

        assertEquals("age (-1) is not in correct format (should be positive integer)", exc.getErrors().get("age"));
    }

    @Test
    void validateBadContentCustomer() {
        Map<String, String> customer = Stream.of(new String[][]{
                {"firstName", "Luke"},
                {"lastName", "Skyfiswalker"},
                {"age", "31"},
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

        ValidationException exc = Assertions.assertThrows(ValidationException.class, () -> validator.validate(customer));

        assertEquals("Contains bad content (fis)", exc.getErrors().get("lastName"));
    }
}