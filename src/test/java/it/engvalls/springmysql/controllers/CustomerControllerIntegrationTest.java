package it.engvalls.springmysql.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.engvalls.springmysql.models.Customer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureMockMvc
class CustomerControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private final Customer CORRECT_CUSTOMER = new Customer(1, "Luke", "Skywalker", 31);

    @Test
    void shouldReturnEmptyListAndStatusSuccess() throws Exception {
        // when get endpoint customers with no customers
        MvcResult result = getCustomers(status().isOk());

        // then get json with empty list as data and status is success
        assertEquals("{\"data\":[],\"status\":\"success\"}", result.getResponse().getContentAsString());
    }

    @Test
    void shouldPostOneCorrectCustomerAndGetCustomerAndStatusSuccess() throws Exception {
        // when posting one customer to endpoint customers
        MvcResult result = postCustomer(objectMapper.writeValueAsString(CORRECT_CUSTOMER), status().isCreated());

        // then get json response with one customer as data and status is success
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONObject data = json.getJSONObject("data");
        assertEquals(CORRECT_CUSTOMER.getFirstName(), data.getString("firstName"));
        assertEquals(CORRECT_CUSTOMER.getLastName(), data.getString("lastName"));
        assertEquals(CORRECT_CUSTOMER.getAge(), data.getInt("age"));

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void shouldGetListWithOneCorrectCustomerAndStatusSuccess() throws Exception {
        // when posting one customer to endpoint customers
        postCustomer(
                "{\"firstName\":\"Luke\",\"lastName\":\"Skywalker\",\"age\":31}",
                status().isCreated()
        );

        // and when get endpoint customers
        MvcResult result = getCustomers(status().isOk());

        // then get json response with list of one customer as data and status is success
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONArray data = json.getJSONArray("data");
        assertEquals(1, data.length());

        JSONObject customer = data.getJSONObject(0);
        assertEquals(CORRECT_CUSTOMER.getFirstName(), customer.getString("firstName"));
        assertEquals(CORRECT_CUSTOMER.getLastName(), customer.getString("lastName"));
        assertEquals(CORRECT_CUSTOMER.getAge(), customer.getInt("age"));

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void shouldGetListWithTwoCorrectCustomerAndStatusSuccess() throws Exception {
        // when posting one customer to endpoint customers
        postCustomer(
                "{\"firstName\":\"Luke\",\"lastName\":\"Skywalker\",\"age\":31}",
                status().isCreated()
        );

        postCustomer(
                "{\"firstName\":\"Leia\",\"lastName\":\"Organa\",\"age\":31}",
                status().isCreated()
        );

        // and when get endpoint customers
        MvcResult result = getCustomers(status().isOk());

        // then get json response with list of two customer as data and status is success
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONArray data = json.getJSONArray("data");
        assertEquals(2, data.length());

        String status = json.getString("status");
        assertEquals("success", status);
    }

    @Test
    void shouldPostOneWrongAgeCustomerAndGetMessageAndStatusFail() throws Exception {

        Customer WRONG_AGE_CUSTOMER = new Customer(1, "Luke", "Skywalker", -1);

        // when posting one customer with wrong age to endpoint customers
        MvcResult result = postCustomer(
                "{\"firstName\":\"Luke\",\"lastName\":\"Skywalker\",\"age\":-1}",
                status().isBadRequest()
        );

        // then get json response with 'age: age (-1) is not in correct format (should be positive integer)'
        // as message and status is fail
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONObject data = json.getJSONObject("message");
        assertEquals("age (-1) is not in correct format (should be positive integer)", data.get("age"));

        String status = json.getString("status");
        assertEquals("fail", status);
    }

    @Test
    void shouldPostOneBadContentCustomerAndGetMessageAndStatusFail() throws Exception {
        // when posting one customer with bad content in lastName to endpoint customers
        MvcResult result = postCustomer(
                "{\"firstName\":\"Luke\",\"lastName\":\"Skyfiswalker\",\"age\":31}",
                status().isBadRequest()
        );

        // then get json response with 'lastName: Contains bad content (fis)" as message and status is fail
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONObject data = json.getJSONObject("message");
        assertEquals("Contains bad content (fis)", data.get("lastName"));

        String status = json.getString("status");
        assertEquals("fail", status);
    }

    @Test
    void shouldPostOneMissingParameterCustomerAndGetMessageAndStatusFail() throws Exception {
        // when posting one customer with missing lastName to endpoint customers
        MvcResult result = postCustomer(
                "{\"firstName\":\"Luke\",\"age\":31}",
                status().isBadRequest()
        );

        // then get json response with 'lastName: Missing parameter' as message and status is fail
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONObject data = json.getJSONObject("message");
        assertEquals("Missing parameter", data.get("lastName"));

        String status = json.getString("status");
        assertEquals("fail", status);
    }

    @Test
    void shouldPostOneMissingValueCustomerAndGetMessageAndStatusFail() throws Exception {
        // when posting one customer with missing value for lastName to endpoint customers
        MvcResult result = postCustomer(
                "{\"firstName\":\"Luke\",\"lastName\":\"\",\"age\":31}",
                status().isBadRequest()
        );

        // then get json response with 'lastName: Missing value' as message and status is fail
        String responseContent = result.getResponse().getContentAsString();
        JSONObject json = new JSONObject(responseContent);

        JSONObject data = json.getJSONObject("message");
        assertEquals("Missing value", data.get("lastName"));

        String status = json.getString("status");
        assertEquals("fail", status);
    }

    private MvcResult getCustomers(ResultMatcher resultMatcher) throws Exception {
        RequestBuilder getRequest = MockMvcRequestBuilders
                .get("/customers")
                .accept(MediaType.APPLICATION_JSON);
        return mvc.perform(getRequest).andExpect(resultMatcher).andReturn();
    }

    private MvcResult postCustomer(String jsonCustomer, ResultMatcher resultMatcher) throws Exception {
        RequestBuilder postRequest = MockMvcRequestBuilders
                .post("/customers")
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonCustomer)
                .contentType(MediaType.APPLICATION_JSON);
        return mvc.perform(postRequest).andExpect(resultMatcher).andReturn();
    }

}